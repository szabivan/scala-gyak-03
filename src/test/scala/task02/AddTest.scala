package task02

import org.scalatest.flatspec.AnyFlatSpec

import java.io.ByteArrayOutputStream
import Rational._

class AddTest extends AnyFlatSpec {

  "Racionálisok összege" should "konstansokra" in {
    val res = add(Rat(1,2),Rat(3,4))
    assert(res.szamlalo.toDouble / res.nevezo == 5.0/4.0, s"1/2 + 3/4 == 5/4 kéne legyen!")
  }
  
  "Racionálisok különbsége" should "konstansokra" in {
    val res = sub(Rat(1,2),Rat(3,4))
    assert(res.szamlalo.toDouble / res.nevezo == -1.0/4.0, s"1/2 - 3/4 == -1/4 kéne legyen!")
  }
  
  "Racionálisok szorzata" should "konstansokra" in {
    val res = mul(Rat(1,2),Rat(3,4))
    assert(res.szamlalo.toDouble / res.nevezo == 3.0/8.0, s"1/2 * 3/4 == 3/8 kéne legyen!")
  }
  
  "Racionálisok hányadosa" should "konstansokra" in {
    val res = div(Rat(1,2),Rat(3,4))
    assert(res.szamlalo.toDouble / res.nevezo == 2.0/3.0, s"1/2 / 3/4 == 2/3 kéne legyen!")
  }
  
  "Racionálisok ellentettje" should "konstansokra" in {
    val res = ellentett(Rat(1,2))
    assert(res.szamlalo.toDouble / res.nevezo == -1.0/2.0, s"1/2 ellentettje -1/2 kéne legyen!")    
  }
  
  "Racionálisok előjele" should "konstansokra" in {
    for( szamlalo <- -2 to 2; nevezo <- -2 to 2 if nevezo != 0 ) {
      val res = isPositive(Rat(szamlalo,nevezo))
      val expected = szamlalo * nevezo > 0
      assert(res == expected, s"$szamlalo / $nevezo esetében isPositive $expected kéne legyen!")
    }
  }

}
