package task00

import org.scalatest.flatspec.AnyFlatSpec

import scala.util.Random
import RangeOps._

class PrimeTest extends AnyFlatSpec {
  "Prím" should "konstansokra" in {
    assert( isPrime( -7 ) == false, "isPrime( -7 ) == false kellene legyen!"  )
    assert( isPrime( 0 ) == false, "isPrime( 0 ) == false kellene legyen!"  )
    assert( isPrime( 1 ) == false, "isPrime( 1 ) == false kellene legyen!"  )
    assert( isPrime( 2 ) == true, "isPrime( 2 ) == true kellene legyen!"  )
    assert( isPrime( 7 ) == true, "isPrime( 7 ) == true kellene legyen!"  )
    assert( isPrime( 19 ) == true, "isPrime( 19 ) == true kellene legyen!"  )
  }
  it should "összeszámolva" in {
    assert( (1 to 100).count( isPrime ) == 25, "1 és 100 közt 25 prímszám van!" )
  }
}

