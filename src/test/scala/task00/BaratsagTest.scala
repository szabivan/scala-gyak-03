package task00

import org.scalatest.flatspec.AnyFlatSpec

import java.io.ByteArrayOutputStream
import RangeOps._

class BaratsagTest extends AnyFlatSpec {

  "Barátságos számok" should "barátságos számok teszt " in {
    val stream = new ByteArrayOutputStream()
    Console.withOut(stream){
      printKoolajVezetek(15000,50000)
    }
    val sep = System.lineSeparator()
    val res = stream.toString
    val actual = s"17296 18416${sep}"
    assert(res == actual, s"15.000 és 50.000 közt egy barátságos számpár van, az output pedig ${res}")
  }
}
