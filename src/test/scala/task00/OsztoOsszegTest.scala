package task00

import org.scalatest.flatspec.AnyFlatSpec

import java.io.ByteArrayOutputStream

import RangeOps._

class OsztoOsszegTest extends AnyFlatSpec {
  
  "Osztóösszeg" should "konstansokra" in {
    for( n <- 1 to 200 ) {
      val res = osztoOsszeg(n)
      val actual = ((1 until n) filter (n%_ == 0)).sum
      assert( res == actual, s"osztoOsszeg($n) == $actual kéne legyen, lett $res")
    }
  }

}
