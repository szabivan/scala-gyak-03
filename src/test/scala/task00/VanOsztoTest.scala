package task00

import org.scalatest.flatspec.AnyFlatSpec

import scala.util.Random
import RangeOps._

class VanOsztoTest extends AnyFlatSpec {
  "Van osztó" should "konstansokra" in {
    assert( vanOszto( 4, 9 ) == true, "vanOszto( 4, 9 ) == true kellene legyen!" )
    assert( vanOszto( 3, 27 ) == false, "vanOszto( 3, 27 ) == false kellene legyen!" )
    assert( vanOszto( 50, 41 ) == false, "vanOszto( 50, 41 ) == false kellene legyen!" )
  }
  it should "generált osztókra" in {
    for( _ <- 1 to 100 ) {
      val o = Random.nextInt(100) + 2
      val n = o * ( Random.nextInt(10) + 2 )
      val k = o + Random.nextInt(10) + 1
      assert( vanOszto( k, n ) == true, s"vanOszto( $k, $n ) == true kellene legyen!" )
    }
  }  
}

