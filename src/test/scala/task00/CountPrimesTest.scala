package task00

import org.scalatest.flatspec.AnyFlatSpec

import scala.util.Random
import RangeOps._

class CountPrimesTest extends AnyFlatSpec {
  "CountPrimes" should "konstansokra" in {
    val theMap = Map( -100 -> 0, 0 -> 0, 100 -> 25, 1000 -> 168 )
    for( (lower, lowerCount) <- theMap ; (upper, upperCount) <- theMap ) {
      if( lower < upper ) assert( countPrimes(lower,upper) == theMap(upper) - theMap(lower), s"$lower és $upper közt ${theMap(upper)-theMap(lower)} prímszám van!" )
    }
  }
}

