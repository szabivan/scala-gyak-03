package task00

import org.scalatest.flatspec.AnyFlatSpec

import scala.util.Random
import RangeOps._

class FizzBuzzTest extends AnyFlatSpec {
  "FizzBuzz" should "konstansokra" in {
    val expectedMap = Map( 7 -> "FizzBuzz", 17 -> "Fizz", 27 -> "Fizz", 77 -> "FizzBuzz", 14 -> "Buzz", 21 -> "Buzz", 33 -> "33" )
    for( (input, expected) <- expectedMap ) {
      assert( fizzBuzz( input ) == expected, s"fizzBuzz( $input ) == $expected kéne legyen!" )
    }
  }
}

