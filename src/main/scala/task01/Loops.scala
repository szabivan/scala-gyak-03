package task01

import scala.annotation.tailrec

/**
 * Próbálj idiomatikus Scala kódot írni, pl. ne használj mutable változókat (var), sem returnt.
 * 
 * Ebben a taskban próbáld megírni a feladatokat tail rekurzív "ciklussal"!
 */

object Loops extends App {
  /**
   * A Fibonacci számok: a 0. a 0, az 1. az 1 és minden további az előző kettő összege.
   * (tehát a sorozat így kezdődik: 0, 1, 1, 2, 3, 5, 8, 13, ...)
   * Írj függvényt, ami kiszámítja az n. Fibonacci számot!
   * Ügyelj arra, hogy az inputként érkező n nagy is lehet.
   * Scalában a BigInt egy akárhány bites egészt tárolni képes típus, ebben add vissza az eredményt!
   * (Ugyanúgy lehet pl. összeadni, mint az Inteket.)
   */
   def fibonacci(n: Int): BigInt = ???
}
