package task00

import Math._
import scala.annotation.tailrec

/**
 * Próbálj idiomatikus Scala kódot írni, pl. ne használj mutable változókat (var), sem returnt.
 * 
 * Ebben a taskban próbáld megírni a feladatokat a Range objektumokkal és metódusaikkal
 * (foreach, exists, filter, count stb)!
 */
object Megoldasok extends App {

  /**
   *  Írj függvényt, ami visszaadja, hogy az input n számnak van-e az input k számnál kisebb valódi osztója!
   *  Pl. vanOszto(4, 9) == true (mert a 3 valódi osztója 9-nek) és vanOszto(3, 27) == false
   *  (mert 27-nek csak 3 és 9 a valódi osztói, amik nem kisebbek, mint 3)
   *  Feltehető, hogy n,k >= 1.
   */
  def vanOszto(k: Int, n: Int): Boolean = (2 until Math.min(k, 1 + Math.sqrt(n).toInt)) exists ( n % _ == 0)

  /**
   *
   *  Akár felhasználva az előző függvényt, írj egy függvényt, ami visszaadja, hogy az input szám prím-e!
   *  Itt ügyelj a negatív számokra, nullára stb. is.
   */
  def isPrime(n: Int): Boolean = 2 <= n && ((2 until n) forall ( n % _ != 0 ))

  /**
   *  Akár felhasználva az előző függvényeket, írj egy függvényt, ami visszaadja, hogy az input a és b
   *  számok közt hány a <= p <= b  prímszám van! (Ismét ügyelj arra, hogy valamelyik végpont lehet negatív is.)
   *  Ha rekurzív függvényt írsz, próbáld meg úgy megírni, hogy olyan paramétert ne adjon át, ami sosem változik!
   */
  def countPrimes(a: Int, b: Int): Int = (a to b) count isPrime
  
  /**
   *  Írj függvényt, ami kap egy n >= 1 Int-et és visszaad egy String-et:
   *  - ha n 7-re végződik, de nem osztható 7-tel, akkor "Fizz"
   *  - ha n osztható 7-tel, de nem 7-re végződik, akkor "Buzz"
   *  - ha n osztható 7-tel és 7-re végződik, akkor "FizzBuzz"
   *  - ha n se 7-tel nem osztható és nem is 7-re végződik, akkor magát a számot Stringként (tízes számrendszerben)!
   *  hint: ugyanúgy lehet az Intet Stringre konvertálni ebben az utolsó esetben, mint ahogy az Intet Longra tettük.
   */
  def fizzBuzz(n: Int): String = (n % 7, n % 10) match {
    case (0, 7) => "FizzBuzz"
    case (_, 7) => "Fizz"
    case (0, _) => "Buzz"
    case _ => n.toString
  }
  
 
  /**
   * Írj függvényt, ami kap egy a és egy b Int-et és az összes a <= n <= b számra növekvő sorrendben
   * hívja a fizzBuzz függvényt, és kiírja a konzolra az eredményt!
   * (Ehhez a taskhoz nincs teszt.)
   */ 
  def printFizzBuzz(a: Int, b: Int): Unit = (a to b) foreach println
  
  /**
   * Írjunk olyan függvényt, ami visszaadja, hogy az input szám
   * nála kisebb osztóinak mennyi az összege! (Pl. 6-ra 1+2+3 = 6,
   * 9-re 1+3=4, 1-re 0 kell legyen az eredmény.)
   * Feltehetjük, hogy az input pozitív egész.
   */
  def osztoOsszeg(n: Int): Int =
    (1 until n)
      .filter(n % _ == 0)
      .sum

  // ehelyett rekurzióval      
  def osztoOsszeg2(n: Int): Int = {
  
    @tailrec
    def helper(i: Int, sum: Int): Int =
      if (i >= n) sum
      else if (n % i == 0) helper(i + 1, sum + i)
      else helper(i + 1, sum)
      
    helper(1, 0)
  }

  /**
   * Írjunk olyan függvényt, ami kap két Intet, 1 <= from <= to,
   * és kiírja konzolra az összes olyan "a b" párt, ahol
   * from <= a <= to, a osztóösszege b és b osztóösszege a!
   * (Ezeket hívják "barátságos" számoknak.
   * Minden pár közt legyen egy szóköz és utánuk egy újsor karakter.
   * Pl. 220 és 284 barátságos számok.
   */
  def printKoolajVezetek(from: Int, to: Int): Unit =
    for (a <- from to to) {
      val b = osztoOsszeg(a)
      if (a <= b && b <= to && osztoOsszeg(b) == a ) println(s"$a $b")
    }
}
